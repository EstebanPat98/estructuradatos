﻿using System;
using System.CodeDom;
using System.Collections.Specialized;

namespace Proyecto
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int opc2 = 0;
            int opc4 = 0;
            do
            {
                int opc = 0, opc1 = 0;
                int opc3 = 0;

                Console.WriteLine("---------MENU--------");
                Console.WriteLine("Que desea consultar \n1. Algoritmos de busqueda \n2. Algoritmos de Ordenamientos");
                opc1 = int.Parse(Console.ReadLine());
                do
                {
                    int[] numeros = { 2, 78, 9, 7, 12, 95, 6, 1, 3, 0, 84, 56, 59, 44, 99, 15, 28, 36, 67, 22 };
                    if (opc1 == 1)
                    {
                        Console.WriteLine("Que opcion deseas realizar \n1. Secuencial \n2. Dicotomicos");
                        opc3 = int.Parse(Console.ReadLine());
                        if (opc3 == 1)
                        {
                            int a, i = 0, pos = 1;
                            bool encontro = false;

                            Console.Write("Favor de ingresar un numero: ");
                            a = int.Parse(Console.ReadLine());

                            while (!(encontro) && i < numeros.Length)
                            {
                                if (a == numeros[i])
                                {
                                    encontro = true;
                                    pos = i;
                                }
                                i = i + 1;
                            }
                            if (encontro)
                                Console.WriteLine("El dato se encuentra y esta en la posicion: " + i);
                            else
                            {
                                Console.WriteLine("El dato no se encuentra");
                            }
                            encontro = false;
                            Console.ReadKey();
                        }
                        if (opc3 == 2)
                        {
                            int a;
                            Console.Write("Favor de ingresar un numero: ");
                            a = int.Parse(Console.ReadLine());

                            BusquedaDicotomicos(numeros, a);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.WriteLine("El numero ingresado no es valido");
                        }
                    }
                    Console.Clear();
                    if (opc1 == 2)
                    {
                        Console.WriteLine("Que opcion deseas realizar \n1. Burbuja \n2. Baraja \n3. Shell \n4. QuickSort  \n5. Radix \n6. Binario \n7. HeapSort");
                        Console.WriteLine("---------------------");
                        opc = int.Parse(Console.ReadLine());
                        Console.Clear();

                        if (opc == 1)
                        {
                            Console.Title = "Burbuja";

                            Console.WriteLine("Antes de ordenar en Metodo Burbuja :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoBurbuja(numeros);

                            Console.WriteLine("Después de ordenar en Metodo Burbuja :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 2)
                        {
                            Console.Title = "Baraja o Insercion directa";

                            Console.WriteLine("Antes de Ordenar en Metodo Baraja :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoBaraja(numeros);

                            Console.WriteLine("Después de ordenar en Metodo Baraja :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 3)
                        {
                            Console.Title = "Shell";

                            Console.WriteLine("Antes de Ordenar en Metodo Shell :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoShell(numeros);

                            Console.WriteLine("Después de ordenar en Metodo Shell :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 4)
                        {
                            Console.Title = "Metodo QuickSort";

                            Console.WriteLine("Antes de Ordenar en Metodo QuickSort :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoQuickSort(numeros, 0, numeros.Length - 1);

                            Console.WriteLine("Despues de Ordenar en Metodo QuickSort :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 5)
                        {
                            Console.Title = "Metodo Radix";

                            Console.WriteLine("Antes de Ordenar en Metodo Redix :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            MetodoRadix(numeros);

                            Console.WriteLine("\nDespues de Ordenar en Metodo Redix :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 6)
                        {
                            Console.Title = "Binario";

                            Console.WriteLine("Antes de Ordenar en Metodo binario :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoBinario(numeros);

                            Console.WriteLine("Despues de Ordenar en Metodo binario :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        if (opc == 7)
                        {
                            Console.Title = "Metodo HeapSort";

                            Console.WriteLine("Antes de Ordenar en Metodo HeapSort :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                            Console.WriteLine("");

                            MetodoHeapSort(numeros);

                            Console.WriteLine("Despues de Ordenar en Metodo HeapSort :");
                            foreach (var numero in numeros)
                            {
                                Console.Write(numero + ",");
                            }
                        }
                        else if(opc >= 8 )
                        {
                            Console.WriteLine("**El numero ingresado es incorrecto");
                        }
                        Console.WriteLine("");
                        Console.WriteLine("\nDesea consultar otro metodo de ordenamiento \n1-.Si \n2-.No");
                        opc2 = int.Parse(Console.ReadLine());
                        if (opc2 == 1)
                        {
                            Console.Clear();
                        }
                    }
                    else
                    {
                        Console.WriteLine("--El numero ingresado es incorrecto");
                    }
                } while (opc2 == 1);
                Console.WriteLine("");
                Console.WriteLine("\nDesea realizar otra opcion \n1-.Si \n2-.No");
                opc4 = int.Parse(Console.ReadLine());
                if (opc4 == 1)
                {
                    Console.Clear();
                }
            } while (opc4 == 1);
        }
        static void BusquedaDicotomicos(int[] arreglo, int a)
        {
            int l = 0, h = 9;
            int m = 0;
            bool found = false;

            while (l <= h && found == false)
            {
                m = (l + h) / 2;
                if (arreglo[m] == a)
                    found = true;
                if (arreglo[m] > a)
                    h = m - 1;
                else
                    l = m + 1;
            }
            if (found == false)
            { Console.Write("\nEl elemento {0} no esta en el arreglo", a); }
            else
            { Console.Write("\nEl elemento {0} esta en la posicion: {1}", a, m + 1); }
        }
        static void MetodoBurbuja(int[] arreglo)
        {
            int i = 0, j = 0, sig = 0, temp = 0;
            for (i = 0; i < arreglo.Length; i++) {
                for (j = 0; j < arreglo.Length - 1; j++)
                {
                    sig = j + 1;

                    if (arreglo[j] > arreglo[sig])
                    {
                        temp = arreglo[j];
                        arreglo[j] = arreglo[sig];
                        arreglo[sig] = temp;
                    }
                }
            }
        }
        static void MetodoBaraja(int[] arreglo)
        {
            int i = 0, j = 0, p = 0;
            for (i = 1; i < arreglo.Length; i++)
            {
                j = arreglo[i];
                p = i - 1;

                while (j < arreglo[p] && p >= 1)
                {
                    arreglo[p + 1] = arreglo[p];
                    p = p - 1;
                }
                if (arreglo[p] <= j)
                    arreglo[p + 1] = j;
                else
                {
                    arreglo[p + 1] = arreglo[p];
                    arreglo[p] = j;
                }
            }
        }

        static void MetodoShell(int[] arreglo)
        {
            int salto = 0, sw = 0, aux = 0, e = 0;
            salto = arreglo.Length / 2;

            while (salto > 0)
            {
                sw = 1;
                while (sw != 0)
                {
                    sw = 0;
                    e = 1;
                    while (e <= (arreglo.Length - salto))
                    {
                        if (arreglo[e - 1] > arreglo[(e - 1) + salto])
                        {
                            aux = arreglo[(e - 1) + salto];
                            arreglo[(e - 1) + salto] = arreglo[e - 1];
                            arreglo[(e - 1)] = aux;
                            sw = 1;
                        }
                        e++;
                    }
                }
                salto = salto / 2;
            }
        }
        static void MetodoQuickSort(int[] arreglo, int izq, int der)
        {
            if (izq < der)
            {
                int part = particion(arreglo, izq, der);
                MetodoQuickSort(arreglo, izq, part);
                MetodoQuickSort(arreglo, part + 1, der);
            }
        }
        static int particion(int[] arreglo, int izq, int der)
        {
            int pivote = arreglo[izq];
            while (true)
            {
                while (arreglo[izq] < pivote)
                {
                    izq++;
                }
                while (arreglo[der] > pivote)
                {
                    der--;
                }
                if (izq >= der)
                {
                    return der;
                }
                else
                {
                    int temp = arreglo[izq];
                    arreglo[izq] = arreglo[der];
                    arreglo[der] = temp;
                    der--;
                    izq++;
                }
            }
        }
        static void MetodoRadix(int[] arreglo)
        {
            int i, j;
            int x;
            int[] temp = new int[arreglo.Length];
            for (x = 31; x > -1; --x)
            {
                j = 0;
                for (i = 0; i < arreglo.Length; ++i)
                {
                    bool m = (arreglo[i] << x) >= 0;
                    if (x == 0 ? !m : m)
                        arreglo[i - j] = arreglo[i];
                    else
                        temp[j++] = arreglo[i];
                }
                Array.Copy(temp, 0, arreglo, arreglo.Length - j, j);
            }
        }
        static void MetodoBinario(int[] arreglo)
        {
            int aux = 0, q = 0, izq = 0, der;
            int i = 0, j = 0, Longitud = 0;

            for (i = 0; i < arreglo.Length; i++)
            {
                aux = arreglo[i];
                der = i - 1;

                while (izq <= der)
                {
                    q = ((izq + der) / 2);
                    if (aux < arreglo[q])
                    {
                        der = q - 1;
                    }
                    else
                    {
                        izq = q + 1;
                    }
                    j = i - 1;
                    while (j >= izq)
                    {
                        arreglo[j + 1] = arreglo[j];
                        j = j - 1;
                    }
                    arreglo[izq] = aux;
                }
            }
            for (i = 0; i < Longitud; i++)
                Console.WriteLine("" + arreglo[i]);
        }
        static void Ajustar(int[] arreglo, int x, int n)
        {
            int temp, j, i = 0;            
            temp = arreglo[i];
            j = 2 * x;
            try
            {
                while (j <= n)
                {
                    if (j < n && arreglo[j] < arreglo[j + 1])
                        j++;
                    if (temp >= arreglo[j])
                        break;
                    arreglo[j / 2] = arreglo[j];
                    j *= 2;
                }
                arreglo[j / 2] = temp;

                Console.ReadKey();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Array Out of bounds", e);
            }
            Console.ReadKey();
        }
        static void MetodoHeapSort(int[] arreglo)
        {
            int i, temp;
            for (i = 0; i >= 0; i--)
            {
                Ajustar(arreglo, i, 21);
            }
            for (i = 8; i > 0; i++)
            {
                temp = arreglo[i + 1];
                arreglo[i + 1] = arreglo[0];
                arreglo[0] = temp;
                Ajustar(arreglo, 21, i);
            }
            for (int x = 0; x < 10; x++)
            {
                Console.WriteLine("{0}", arreglo[x]);
            }
            Console.ReadKey();
        }
    }
}
